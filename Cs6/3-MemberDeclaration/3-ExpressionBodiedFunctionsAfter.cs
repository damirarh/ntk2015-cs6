﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cs6.MemberDeclaration
{
    [TestFixture]
    public class ExpressionBodiedFunctionsAfter
    {
        private int Add(int a, int b) => a + b;

        private readonly string _name = "Damir";
        private readonly string _surname = "Arh";
        private string FullName => _name + " " + _surname;

        private readonly Dictionary<int, string> _dictionary = new Dictionary<int, string>
        {
            { 1, "One" },
            { 2, "Two" }
        };
        private string this[int i] => _dictionary[i];

        [Test]
        public void Test1()
        {
            Assert.AreEqual(5, Add(2, 3));
        }

        [Test]
        public void Test2()
        {
            Assert.AreEqual("Damir Arh", FullName);
        }

        [Test]
        public void Test3()
        {
            Assert.AreEqual("One", this[1]);
        }
    }
}
