﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cs6.ExpressionLevel
{
    [TestFixture]
    public class IndexInitializers
    {
        private Dictionary<string, string> GetDictionary()
        {
            return new Dictionary<string, string>
            {
                { "Name", "Damir" }
            };
        }

        [Test]
        public void Test()
        {
            Assert.AreEqual("Damir", GetDictionary()["Name"]);
        }
    }
}
