﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cs6.MemberDeclaration
{
    [TestFixture]
    public class AutoPropertyInitializerAfter
    {
        public string Name { get; set; } = "Damir";

        [Test]
        public void Test()
        {
            Assert.AreEqual("Damir", Name);
        }
    }
}
