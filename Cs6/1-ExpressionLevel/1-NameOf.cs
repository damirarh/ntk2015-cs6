﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cs6.ExpressionLevel
{
    [TestFixture]
    public class NameOf
    {
        private void Process(object arg)
        {
            if (arg == null)
            {
                throw new ArgumentNullException("arg");
            }
        }

        [Test]
        public void Test()
        {
            var exception = Assert.Throws<ArgumentNullException>(() => Process(null));
            Assert.AreEqual("arg", exception.ParamName);
        }
    }
}
