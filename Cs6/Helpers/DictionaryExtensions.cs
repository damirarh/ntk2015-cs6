﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cs6.Helpers
{
    public static class DictionaryExtensions
    {
        public static void Add(this Dictionary<Type, string> dictionary, Type type)
        {
            dictionary.Add(type, type.FullName);
        }
    }
}
