﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cs6.MemberDeclaration
{
    [TestFixture]
    public class GetterOnlyAutoProperties
    {
        private readonly string _name = "Damir";
        private readonly string _surname;

        public GetterOnlyAutoProperties()
        {
            _surname = "Arh";
        }

        public string Name
        {
            get { return _name; }
        }

        public string Surname
        {
            get { return _surname; }
        }

        [Test]
        public void Test1()
        {
            Assert.AreEqual("Damir", Name);
        }

        [Test]
        public void Test2()
        {
            Assert.AreEqual("Arh", Surname);
        }
    }
}
