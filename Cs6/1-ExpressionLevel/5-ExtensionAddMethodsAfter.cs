﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cs6.Helpers;
using NUnit.Framework;

namespace Cs6.ExpressionLevel
{
    [TestFixture]
    public class ExtensionAddMethodsAfter
    {
        private Dictionary<Type, string> GetDictionary()
        {
            return new Dictionary<Type, string>
            {
                typeof(string)
            };
        }

        [Test]
        public void Test()
        {
            Assert.AreEqual("System.String", GetDictionary()[typeof(string)]);
        }
    }
}
