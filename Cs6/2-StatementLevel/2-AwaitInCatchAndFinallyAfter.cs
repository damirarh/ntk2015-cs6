﻿using Cs6.Helpers;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cs6.StatementLevel
{
    [TestFixture]
    public class AwaitInCatchAndFinallyAfter
    {
        private async Task HandleAsync(AsyncResource resource, bool throwException)
        {
            try
            {
                await resource.OpenAsync(throwException);
            }
            catch (Exception exception)
            {
                await resource.LogAsync(exception);
            }
            finally
            {
                await resource.CloseAsync();
            }
        }

        [Test]
        public async Task Test1()
        {
            var resource = new AsyncResource();
            await HandleAsync(resource, false);
            Assert.IsTrue(resource.Opened);
            Assert.IsFalse(resource.Logged);
            Assert.IsTrue(resource.Closed);
        }

        [Test]
        public async Task Test2()
        {
            var resource = new AsyncResource();
            await HandleAsync(resource, true);
            Assert.IsTrue(resource.Opened);
            Assert.IsTrue(resource.Logged);
            Assert.IsTrue(resource.Closed);
        }
    }
}
