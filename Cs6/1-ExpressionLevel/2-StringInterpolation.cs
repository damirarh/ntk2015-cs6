﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cs6.ExpressionLevel
{
    [TestFixture]
    class StringInterpolation
    {
        private string GetFormattedPrice(decimal price)
        {
            return string.Format("{0:0.00} €",   price);
        }

        [Test]
        public void Test()
        {
            Assert.AreEqual("10,00 €", GetFormattedPrice(10));
        }
    }
}
