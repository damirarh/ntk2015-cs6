﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cs6.Helpers
{
    public class AsyncResource
    {
        public bool Opened { get; set; }
        public bool Logged { get; set; }
        public bool Closed { get; set; }

        public async Task OpenAsync(bool throwException)
        {
            Opened = true;
            await Task.Yield();
            if (throwException)
            {
                throw new Exception();
            }
        }

        public async Task LogAsync(Exception e)
        {
            Logged = true;
            await Task.Yield();
        }

        public async Task CloseAsync()
        {
            Closed = true;
            await Task.Yield();
        }
    }
}
