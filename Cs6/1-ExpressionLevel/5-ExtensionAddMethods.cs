﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cs6.Helpers;
using NUnit.Framework;

namespace Cs6.ExpressionLevel
{
    [TestFixture]
    public class ExtensionAddMethods
    {
        private Dictionary<Type, string> GetDictionary()
        {
            var dictionary = new Dictionary<Type, string>();
            dictionary.Add(typeof(string));
            return dictionary;
        }

        [Test]
        public void Test()
        {
            Assert.AreEqual("System.String", GetDictionary()[typeof(string)]);
        }
    }
}
