﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

using static System.Linq.Enumerable;
using static System.Math;

namespace Cs6.Import
{
    [TestFixture]
    public class UsingStaticAfter
    {
        [Test]
        public void Test1()
        {
            Assert.AreEqual(100, Pow(10, 2));
        }

        [Test]
        public void Test2()
        {
            Assert.AreEqual(1, new[] { 1, 2, 3 }.Min());
        }
    }
}
