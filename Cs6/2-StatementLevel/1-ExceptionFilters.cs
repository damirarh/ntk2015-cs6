﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cs6.StatementLevel
{
    [TestFixture]
    public class ExceptionFilters
    {
        private void ThrowException(string paramName)
        {
            try
            {
                throw new ArgumentException(null, paramName);
            }
            catch (ArgumentException exception)
            {
                if (exception.ParamName != "ignore")
                {
                    throw;
                }
            }
        }

        [Test]
        public void Test1()
        {
            Assert.DoesNotThrow(() => ThrowException("ignore"));
        }

        [Test]
        public void Test2()
        {
            Assert.Throws<ArgumentException>(() => ThrowException("dontIgnore"));
        }
    }
}
