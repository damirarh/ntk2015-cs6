﻿using Cs6.Helpers;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cs6.StatementLevel
{
    [TestFixture]
    public class AwaitInCatchAndFinally
    {
        private async Task HandleAsync(AsyncResource resource, bool throwException)
        {
            Exception exceptionToLog = null;
            try
            {
                await resource.OpenAsync(throwException);
            }
            catch (Exception exception)
            {
                exceptionToLog = exception;
            }
            if (exceptionToLog != null)
            {
                await resource.LogAsync(exceptionToLog);
            }
        }

        [Test]
        public async Task Test1()
        {
            var resource = new AsyncResource();
            await HandleAsync(resource, false);
            Assert.IsTrue(resource.Opened);
            Assert.IsFalse(resource.Logged);
        }

        [Test]
        public async Task Test2()
        {
            var resource = new AsyncResource();
            await HandleAsync(resource, true);
            Assert.IsTrue(resource.Opened);
            Assert.IsTrue(resource.Logged);
        }
    }
}
