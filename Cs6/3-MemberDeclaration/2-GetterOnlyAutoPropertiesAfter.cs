﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cs6.MemberDeclaration
{
    [TestFixture]
    public class GetterOnlyAutoPropertiesAfter
    {
        public GetterOnlyAutoPropertiesAfter()
        {
            Surname = "Arh";
        }

        public string Name { get; } = "Damir";
        public string Surname { get; }

        [Test]
        public void Test1()
        {
            Assert.AreEqual("Damir", Name);
        }

        [Test]
        public void Test2()
        {
            Assert.AreEqual("Arh", Surname);
        }
    }
}
