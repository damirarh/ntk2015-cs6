﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cs6.ExpressionLevel
{
    [TestFixture]
    public class NullConditionalOperatorsAfter
    {
        private int GetStringLength(string arg)
        {
            return arg?.Length ?? 0;
        }

        [Test]
        public void Test1()
        {
            Assert.AreEqual(0, GetStringLength(null));
        }

        private char? GetFirstChar(string arg)
        {
            return arg?[0];
        }

        [Test]
        public void Test2()
        {
            Assert.AreEqual(null, GetFirstChar(null));
        }

        private bool? InvokeFunc(Func<bool> func)
        {
            return func?.Invoke();
        }

        [Test]
        public void Test3()
        {
            Assert.AreEqual(null, InvokeFunc(null));
        }
    }
}
