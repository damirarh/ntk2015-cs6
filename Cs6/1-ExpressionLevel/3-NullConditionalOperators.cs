﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cs6.ExpressionLevel
{
    [TestFixture]
    public class NullConditionalOperators
    {
        private int GetStringLength(string arg)
        {
            return arg != null ? arg.Length : 0;
        }

        [Test]
        public void Test1()
        {
            Assert.AreEqual(0, GetStringLength(null));
        }

        private char? GetFirstChar(string arg)
        {
            return arg != null ? (char?)arg[0] : null;
        }

        [Test]
        public void Test2()
        {
            Assert.AreEqual(null, GetFirstChar(null));
        }

        private bool? InvokeFunc(Func<bool> func)
        {
            if (func != null)
            {
                return func();
            }
            return null;
        }

        [Test]
        public void Test3()
        {
            Assert.AreEqual(null, InvokeFunc(null));
        }
    }
}
