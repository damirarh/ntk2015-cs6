﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cs6.Import
{
    [TestFixture]
    public class UsingStatic
    {
        [Test]
        public void Test1()
        {
            Assert.AreEqual(100, Math.Pow(10, 2));
        }

        [Test]
        public void Test2()
        {
            Assert.AreEqual(1, new[] { 1, 2, 3 }.Min());
        }
    }
}
